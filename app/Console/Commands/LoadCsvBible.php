<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Helpers\ConvertCsvToCorpus;

class LoadCsvBible extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:csvbible {files*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Load Bible(s) stored in CSV format. You can specify multiple files. You can also use wildcards.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        list ($isValidFileList, $errorMessagesPerPattern, $fileList) = $this->GetGlobbedUniqueFileList($this->argument('files'));

        if (!$isValidFileList) {
            $this->PrintErrorMessagesPerPattern($errorMessagesPerPattern);
            print "No files processed, exiting.\n\n";
            return;
        }

        foreach ($fileList as $filename) {
            try {
                $converter = new ConvertCsvToCorpus($filename);
            } catch (\Exception $e) {
                print "Error opening or processing file '$filename': ".$e->getMessage()."\n";
                continue;
            }

            // If everything went well, only then we write the data to the db
            $converter->SaveCorpus();
        }
    }

    /**
     * Get a list of filenames by globbing the patterns in the argument list and making the list unique
     *
     * @return array [0] (success): boolean, [1] (errorMessagesPerPattern): array, [2] (fileList): array
     */
    private function GetGlobbedUniqueFileList($args) {
        $isValidFileList = true;
        $errorMessages = [];
        $fileList = [];

        foreach ($this->argument('files') as $pattern) {
            $globbedFiles = glob($pattern);

            list($validationResult, $validationMessages) = $this->ValidateFileList($globbedFiles);
            if ($validationResult != 'success') {
                $errorMessages[] = [
                    'pattern' => $pattern,
                    'validationMessages' => $validationMessages
                ];
                $isValidFileList = false;
                continue;
            }

            $fileList = array_merge($fileList, $globbedFiles);
        }

        return [$isValidFileList, $errorMessages, array_unique($fileList)];
    }

    /**
     * Validates a file list
     *
     * @return array [0]: { 'success', 'failure' }, [1] (messages): array[string]
     */
    private function ValidateFileList(Array $fileList) {
        if (count($fileList) < 1) {
            return [ 'failure', [ 'No matching files found' ]];
        }

        $isAllItemsCsv = true;
        $isAllFilenamesValid = true;
        $firstBadFilename = '';
        foreach ($fileList as $filename) {
            if (substr($filename, -4) != '.csv') {
                $isAllItemsCsv = false;
                $firstBadFilename = $filename;
                break 1;
            }
            if (!preg_match('/[^\/]+\.csv$/', $filename)) {
                $isAllFilenamesValid = false;
                $firstBadFilename = $filename;
                break 1;
            }
        }
        if (!$isAllItemsCsv) {
            return [ 'failure', [ 'Non CSV filename found: ' . $firstBadFilename ]];
        }
        if (!$isAllFilenamesValid) {
            return [ 'failure', [ 'Invalid filename found: ' . $firstBadFilename ]];
        }

        return [ 'success', []];
    }

    /**
     * Print error messages given in arrays of arrays, arranged by file name patterns
     *
     * @return void
     */
    private function PrintErrorMessagesPerPattern($errMsgArr) {
        print "Bad argument(s)\n";
        foreach ($errMsgArr as $errMsg) {
            print "- ".$errMsg['pattern'].":\n";
            foreach ($errMsg['validationMessages'] as $e) {
                print "  - $e\n";
            }
        }
    }
}
