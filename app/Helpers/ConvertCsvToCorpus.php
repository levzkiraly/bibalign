<?php

namespace App\Helpers;

use App\Version;
use App\Corpus;

class ConvertCsvToCorpus
{
    /**
     * The input data, and settings
     * @var Array
     */
    private $file;
    private $cellTypes = [
        'book_id',
        'chapter',
        'verse',
        'subverse',
        'text',
        'references',
        'more_references'
    ];
    private $columnCount;

    /**
     * The Version object, and settings
     * @var string
     */
    private $version;
    private $versionNameRegexp = '/^(?:.*\/)?(?P<versionName>[^\/]+)\.csv$/';

    /**
     * The output data: an array of Corpus objects
     * @var Array
     */
    private $corpus = [];

    /**
     * Open a CSV Bible and validate it by the types of the first column
     * If valid, create the Version object
     * Premises:
     * - File must exist
     * - Filename must end with .csv
     * - Filename must have a valid name part before the extension
     */
    public function __construct($filename) {
        $this->file = file($filename);
        $this->ValidateCsvColumns($this->file[0]);
        $this->CreateVersion($filename);
        $this->ReadRows();
    }

    /**
     * ValidateCsvColumns
     *
     * Throw an \Exception if number of columns or any column is invalid
     */
    private function ValidateCsvColumns($strRow) {
        $arrRow = str_getcsv($strRow);

        if (
            count($arrRow) < 4
            || (is_numeric($arrRow[3]) && count($arrRow) < 5) // if arrRow[3] is subverseNumber, then arrRow[4] is the text
        ) {
            throw new \Exception("Too few columns");
        }

        if (!is_numeric($arrRow[3])) {
            array_splice($this->cellTypes, 3, 1); // delete subverseNumber
        }

        if (count($arrRow) > count($this->cellTypes)) {
            throw new \Exception("Too many columns");
        }
        $this->columnCount = count($arrRow);

        if (
            !is_numeric($arrRow[0])
            || !is_numeric($arrRow[1])
            || !is_numeric($arrRow[2])
        ) {
            throw new \Exception("Unrecognizeable location format");
        }
    }

    /**
     * Create a Version object from the filename, parsing it for a short_name.
     */
    private function CreateVersion($filename) {
        preg_match($this->versionNameRegexp, $filename, $matches);
        $this->version = new Version();
        $this->version->short_name = $matches['versionName'];
        $this->version->save();
    }

    /**
     * Process the array of csv rows in $file, and convert them to Corpus objects
     */
    private function ReadRows() {
        foreach ($this->file as $csvRow) {
            $csvRow = trim($csvRow);
            if ($csvRow == '') {
                continue;
            }

            $csvCells = str_getcsv($csvRow);

            $c = new Corpus();
            $c->version_id = $this->version->id;

            for ($i = 0; $i < $this->columnCount; $i++) {
                $c->{$this->cellTypes[$i]} = $csvCells[$i];
            }

            $this->corpus[] = $c;
        }
    }

    /**
     * Save the version and the corpus to the database
     */
    public function SaveCorpus() {
        foreach ($this->corpus as $c) {
            $c->save();
        }
    }
}
