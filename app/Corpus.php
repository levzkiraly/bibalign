<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Version;

class Corpus extends Model
{
    protected $table = 'corpora';
    public $timestamps = false;

    /**
     * Get the version the corpus unit belongs to
     * @return App\Version the Version object
     */
    public function version() {
        return $this->belongsTo('App\Version', 'version_id');
    }
}
