<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .position-ref {
                position: relative;
            }

            #main-frame {
                display: flex;
                flex-flow: column;
                align-items: stretch;
            }
            #title {
                flex: 0 auto;
                font-size: 42px;
                padding-left: 1rem;
            }

            .content {
                flex: 1 1 auto;
                display: flex;
                flex-flow: row nowrap;
                overflow-y: auto;
            }

            #bib-tree {
                flex: 0 0 25%;
                border-top: lightgrey solid 1px;
                display: flex;
                flex-flow: column;
            }

            #bib-info {
                flex: 1;
            }

            .dev-border {
                border: 1px dashed red;
            }

/* ezek kellenek? vvv */
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }
/* ezek kellenek? ^^^ */
        </style>
    </head>
    <body>
        <div id="main-frame" class="position-ref full-height">
            <div id="title">
                BibAlign
            </div>
            <div id="app" class="content">
                <bib-tree id="bib-tree">
                </bib-tree>
                <bib-info id="bib-info" class="dev-border">
                </bib-info>
            </div>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
