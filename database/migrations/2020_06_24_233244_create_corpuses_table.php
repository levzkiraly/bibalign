<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCorpusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('corpora', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('version_id')->unsigned()->nullable(false);
            $table->unsignedInteger('esz')->nullable(true);
            $table->unsignedInteger('book_id')->nullable(false);
            $table->unsignedInteger('chapter')->nullable(false);
            $table->unsignedInteger('verse')->nullable(false);
            $table->unsignedInteger('subverse')->nullable(true);
            $table->string('text')->nullable(false);
            $table->string('references')->nullable(true);
            $table->string('more_references')->nullable(true);

            $table->foreign('version_id')->references('id')->on('versions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('corpora');
    }
}
